/* global Complex */

const testComplexConstructor1 = () => {
    const x = new Complex();

    return x.re === 0 && x.im === 0;
};

const testComplexConstructor2 = x => {
    const y = new Complex(x);

    return y.re === x && y.im === 0;
};

const testComplexConstructor3 = x => {
    const y = new Complex(0, x);

    return y.re === 0 && y.im === x;
};

const testComplexMag1 = x => x.approx(new Complex(x).mag);

const testComplexMag2 = x => x.approx(new Complex(0, x).mag);

const testComplexMag3 = (x, y) => new Complex(x, y).mag >= 0;
