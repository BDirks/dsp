# Goals

1:      V   Play a sine wave using WebAudio.
2:      V   Play a custom sine wave.
3:      V   Play a custom sawtooth wave.
4:      V   Play a custom rectangle wave.
5:      V   Play a custom triangle wave.
6:      V   Rudimentary implementation of windows from Haskell project.
7:      V   Investigate and play the simplest kind of noise.
8:      V   Plot a custom sine wave.
9:      V   Plot FM.
10:     V   Fix winHamming.
11:     V   Multiply two signals.
12:     V   Fix saw FM.
13:     V   Add two signals.
14:     V   Normalize signal in graph.
15:     V   Concat two signals.
16:     V   Scale a signal by a constant.
17:     V   Plot the convolution of two signals.
18:     V   Calculate the dot product of two signals.
19:     V   Smoothen a signal.
20:     V   Add a delay to a signal.
21:     V   Smoothen signal before plotting.
22:     V   Generate Parzen window.
23:     V   Create a binaural beat.
24:     V   Generate generalized normal window.
25:     V   Fix mod.
26:     V   Implement basic TET12.
27:     V   Play an A4 tone.
28:     V   Play two bars from "für Elise".
29:         Synthesize a piano.
30:         Synthesize a guitar.
31:     V   Create ADSR envelope.
32:     V   Play a sound.
33:     V   Show sample & time in graph.
34:     V   Repeat a signal.
35:     V   Plot exponential decay.
36:     V   Plot the RMS.
37:     V   Plot multiple signals.
38:     V   Implement DFT.
39:     V   Fix smoothen.
40:     V   Fix Bartlett window.
41:     V   Plot all windows.
42:     V   Plot SMA and CMA.
43:     V   Never use original signal.
44:     V   Use a noise gate.
45:     V   Fix Brownian noise.
46:     V   Test different methods of plotting.
47:     V   Fix SMA.
48:     V   Synthesize a kick drum.
49:     V   Plot a signal with its DFT.
50:     V   Speed up DFT.
51:     V   Plot a signal with its DFT using FFT.
52:     V   Plot a signal with its DFT and its inverse.
53:     ?   Implement convolution using FFT.
54:     V   Use a window as a filter.
55:     V   Accept Complex32Arrays in mul.
56:     V   Accept Complex32Arrays in add.
57:     V   Add the option to plot peak values.
58:     V   Implement fast mirrored array generation.
59:     ?   Fix FFT linear convolution.
60:     V   Calculate the SD of a signal.
61:     V   Calculate the histogram of a signal.
62:     V   Calculate the mode of a signal.
63:     V   Fix/verify noise gate.
64:     V   Implement ducking.
65:     V   Add ducking attack.
66:     V   Test performance of recursive sma.
67:         Plot the sinc function.
68:     V   Load multiple files.
69:     V   Implement padding.
70:     V   Setup drumset.
71:     V   Implement trigger track.
72:     V   Clean drum1.
73:     V   Implement pulse series.
74:     V   Multi-channel reduce-based sum.
75:     V   Implement proper exponential line generator.
76:     V   Use subtractive synthesis.
77:     V   Benchmark current fft.
78:     V   Implement Complex arithmetic necessary for fft.
79:     V   Test performance impact of using Complex arithmetic operations.
80:     V   Demonstrate Karplus-Strong synthesis.
81:     V   Implement delay.
82:     V   Support custom smoothing period of Karplus-Strong.
83:         Support fractional delay.
84:         Support panning.
85:     V   Implement new player.
86:     V   Support phase modulation.
87:     V   Implement resampling.
88:     V   Implement circular shift.