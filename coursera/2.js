/* globals Complex32Array */

const supported = (X, n) => (0 > n || n >= X.length ? 0 : X[n]);

const f3 = N =>
    Float32Array.generate(N, n => (n >= 1 && n <= 3 ? (-1) ** n * n : 0));

const x3 = f3(4);

const y3 = f3(7);

console.log('3: ' + x3.energy + ' 0 oo ' + y3.energy / 7);

const f4 = N => {
    const X = Float32Array.delta(N);

    return Float32Array.generate(
        N,
        n => supported(X, n) + 2 * supported(X, n - 1) + 3 * supported(X, n - 2)
    );
};

const g4 = N => {
    const X = f4(N);

    return Float32Array.generate(N, n => 0.5 * (X[n] + (X[n - 1] || 0)));
};

const h4 = N => {
    const X = Float32Array.delta(N);

    return Float32Array.generate(
        N,
        n =>
            0.5 * supported(X, n) +
            1.5 * supported(X, n - 1) +
            2.5 * supported(X, n - 2) +
            1.5 * supported(X, n - 3)
    );
};

const y4 = g4(1000);

const z4 = h4(1000);

const a4_1 = y4.every(x => x === 0);

console.log('4.1: ' + a4_1);

const a4_2 = y4.approx(z4);

console.log('4.2: ' + a4_2);

const a5 = 44100 * 60 * 2;

console.log('5: ' + a5);

const f7 = (M, N, Q) => {
    const X = new Complex32Array(Q);

    const a = M / N * TAU;

    for (let n = 0; n < Q; n++) {
        const b = a * n;

        X.re[n] = Math.cos(b);

        X.im[n] = Math.sin(b);
    }

    return X;
};

new Plot([f7(1, 3, 20)], 'a');

new Plot([f7(5, 7, 20)], 'b');

new Plot([f7(15, 35, 20)], 'c');
