const _ = new WebAudio();

const a = (u, v) =>
    Float32Array.sum(
        [2, 3, 5, 7, 11, 13, 17].map(x =>
            Float32Array.white(126).ks({
                N: _.s(4),
                delay: Math.ceil(u + x * 0.5),
                feedback: 1 - (18 - x) / 1000,
                smoothing: v + x * 1
            })
        )
    ).mul(Float32Array.winTukey(_.s(4), 0.01));

window.addEventListener('keydown', e => play(e.keyCode), false);

const play = x => {
    console.log(x);

    // const t = Date.now();

    const z = {
        81: 10 ** 2,
        87: 12 ** 2,
        69: 14 ** 2,
        82: 16 ** 2,
        84: 18 ** 2,
        89: 20 ** 2,
        85: 22 ** 2,
        79: 24 ** 2
    }[x];

    if (z) {
        const q = a(z, 11);

        _.play([q, q]);
    }

    const y = {
        65: 10 ** 2,
        83: 12 ** 2,
        68: 14 ** 2,
        70: 16 ** 2,
        71: 18 ** 2,
        72: 20 ** 2,
        74: 22 ** 2,
        75: 24 ** 2
    }[x];

    if (y) {
        const q = a(y, 3);

        _.play([q, q]);
    }

    // console.log(Date.now() - t);
};
