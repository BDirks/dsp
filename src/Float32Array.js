/* global Check, Complex32Array, Float32Matrix */

const TAU = Math.PI * 2;

const TAU1 = 1 / TAU;

Float32Array.brown = (N, step) => {
    Check.isIndex(N, 'Float32Array.brown: N');

    let a = 0;

    if (Number.isFinite(step)) {
        step = new Float32Array(N).fill(step);
    } else if (!(step instanceof Float32Array)) {
        throw new TypeError(
            '<Float32Array.brown: step> is not a finite number or a Float32Array'
        );
    }

    const stepN = step.length;

    return Float32Array.generate(N, n => {
        const b = step[n % stepN];

        a += Math.random() * b - b / 2;

        return a;
    }).normalize();
};

Float32Array.cos = R => (
    N,
    f,
    {A = 1, fm = new Float32Array(N), p = 0} = {}
) => {
    Check.isIndex(N, 'Float32Array.cos: N');

    Check.isFinite(f, 'Float32Array.cos: f');

    Check.isFloat32Array(fm, 'Float32Array.cos: fm');

    if (Number.isFinite(A)) {
        A = new Float32Array(N).fill(A);
    } else if (!(A instanceof Float32Array)) {
        throw new TypeError(
            '<Float32Array.cos: A> is not a finite number or a Float32Array'
        );
    }

    const AN = A.length;

    const fmN = fm.length;

    if (Number.isFinite(p)) {
        p = new Float32Array(N).fill(p / TAU);
    } else if (A instanceof Float32Array) {
        p = p.mul(TAU1);
    } else {
        throw new TypeError(
            '<Float32Array.cos: p> is not a finite number or a Float32Array'
        );
    }

    const pN = p.length;

    const a = TAU * f / R;

    return Float32Array.generate(
        N,
        n => A[n % AN] * Math.cos(a * n + p[n % pN] + fm[n % fmN])
    );
};

Float32Array.expDecay = (N, a) => {
    Check.isIndex(N, 'Float32Array.expDecay: N');

    Check.isFinite(a, 'Float32Array.expDecay: a');

    if (Math.abs(a) > 1)
        throw new RangeError(
            '<Float32Array.expDecay: a> has an absolute value greater than 1'
        );

    return Float32Array.generate(N, n => a ** n);
};

Float32Array.generate = (N, generator) => {
    Check.isIndex(N, 'Float32Array.generate: N');

    Check.isFunction(generator, 'Float32Array.generate: generator');

    const X = new Float32Array(N);

    for (let n = 0; n < N; n++) X[n] = generator(n);

    return X;
};

Float32Array.generateMirror = (N, generator) => {
    Check.isIndex(N, 'Float32Array.generateMirror: N');

    Check.isFunction(generator, 'Float32Array.generateMirror: generator');

    const M = N / 2;

    const K = N - 1;

    const X = new Float32Array(N);

    for (let n = 0; n < M; n++) X[n] = X[K - n] = generator(n);

    if (N % 2) X[M] = generator[M];

    return X;
};

Float32Array.isBasis = X => Float32Matrix.fromCols(X).det !== 0;

Float32Array.lin = ({end, N, power = 1, start}) => {
    Check.isIndex(N, 'Float32Array.lin: N');

    Check.isFinite(start, 'Float32Array.lin: start');

    Check.isFinite(end, 'Float32Array.lin: end');

    if (start === end) return new Float32Array(N).fill(start);

    if (Number.isFinite(power)) {
        power = new Float32Array(N).fill(power);
    } else if (!(power instanceof Float32Array)) {
        throw new TypeError(
            '<Float32Array.lin: power> is not a finite number or a Float32Array'
        );
    }

    const aN = power.length;

    const d = start - end;

    const M = N ** power.max;

    return Float32Array.generate(N, n => start - d * n ** power[n % aN] / M);
};

Float32Array.orthonormalBasis = N => {
    Check.isIndex(N, 'Float32Array.orthonormalBasis: N');

    const X = new Array(N);

    for (let n = 0; n < N; n++) {
        const Y = new Float32Array(N);

        Y[n] = 1;

        X[n] = Y;
    }

    return X;
};

Float32Array.propAdditiveIdentity = X => {
    Check.isFloat32Array(X, 'Float32Array.propAdditiveIdentity: X');

    X.add(new Float32Array(X.length)).approx(X);
};

Float32Array.propAdditiveInverse = X => {
    Check.isFloat32Array(X, 'Float32Array.propAdditiveInverse: X');

    X.add(X.neg).approx(new Float32Array(X.length));
};

Float32Array.propAssociativeAddition = (X, Y, Z) => {
    Check.isFloat32Array(X, 'Float32Array.propAssociativeAddition: X');

    Check.isFloat32Array(Y, 'Float32Array.propAssociativeAddition: Y');

    Check.isFloat32Array(Z, 'Float32Array.propAssociativeAddition: Z');

    X.add(Y).add(Z).approx(X.add(Y.add(Z)));
};

Float32Array.propCauchySchwarzInequality = (X, Y) => {
    Check.isFloat32Array(X, 'Float32Array.propCauchySchwarzInequality: X');

    Check.isFloat32Array(Y, 'Float32Array.propCauchySchwarzInequality: Y');

    const a = Math.abs(X.dot(Y));

    const b = X.norm * Y.norm;

    return a < b || (a === b && X.approx(Y.mul(a)));
};

Float32Array.propCommutativeAddition = (X, Y) => {
    Check.isFloat32Array(X, 'Float32Array.propCommutativeAddition: X');

    Check.isFloat32Array(Y, 'Float32Array.propCommutativeAddition: Y');

    return X.add(Y).approx(Y.add(X));
};

Float32Array.propDistanceFromDot = (X, Y) => {
    Check.isFloat32Array(X, 'Float32Array.propDistanceFromDot: X');

    Check.isFloat32Array(Y, 'Float32Array.propDistanceFromDot: Y');

    const Z = X.sub(Y);

    return Z.dot(Z).pow(0.5);
};

Float32Array.propDistributiveScalarMultiplication = (a, b, X, Y) =>
    Float32Array.propDistributiveScalarMultiplication1(a, X, Y) &&
    Float32Array.propDistributiveScalarMultiplication2(a, b, X) &&
    Float32Array.propDistributiveScalarMultiplication3(a, b, X);

Float32Array.propDistributiveScalarMultiplication1 = (a, X, Y) => {
    Check.isFinite(a, 'Float32Array.propDistributiveScalarMultiplication1: a');

    Check.isFloat32Array(
        X,
        'Float32Array.propDistributiveScalarMultiplication1: X'
    );

    Check.isFloat32Array(
        Y,
        'Float32Array.propDistributiveScalarMultiplication1: Y'
    );

    return X.add(Y).mul(a).approx(X.mul(a).add(Y.mul(a)));
};

Float32Array.propDistributiveScalarMultiplication2 = (a, b, X) => {
    Check.isFinite(a, 'Float32Array.propDistributiveScalarMultiplication2: a');

    Check.isFinite(b, 'Float32Array.propDistributiveScalarMultiplication2: b');

    Check.isFloat32Array(
        X,
        'Float32Array.propDistributiveScalarMultiplication2: X'
    );

    return X.mul(a + b).approx(X.mul(a).add(X.mul(b)));
};

Float32Array.propDistributiveScalarMultiplication3 = (a, b, X) => {
    Check.isFinite(a, 'Float32Array.propDistributiveScalarMultiplication3: a');

    Check.isFinite(b, 'Float32Array.propDistributiveScalarMultiplication3: b');

    Check.isFloat32Array(
        X,
        'Float32Array.propDistributiveScalarMultiplication3: X'
    );

    return X.mul(b).mul(a).approx(X.mul(a * b));
};

Float32Array.propDotDistributiveAddition = (X, Y) => {
    Check.isFloat32Array(X, 'Float32Array.propDotDistributiveAddition: X');

    Check.isFloat32Array(Y, 'Float32Array.propDotDistributiveAddition: Y');

    return Number.approx(X.add(Y).dot(X), X.dot(X) + Y.dot(X));
};

Float32Array.propDotScalingScalarMultiplication = (a, X, Y) => {
    Check.isFinite(a, 'Float32Array.propDotScalingScalarMultiplication: a');

    Check.isFloat32Array(
        X,
        'Float32Array.propDotScalingScalarMultiplication: X'
    );

    Check.isFloat32Array(
        Y,
        'Float32Array.propDotScalingScalarMultiplication: Y'
    );

    return Number.approx(X.dot(Y.mul(a)), X.dot(Y) * a);
};

Float32Array.propDotSelfPositive = X =>
    Float32Array.propDotSelfPositive1(X) &&
    Float32Array.propDotSelfPositive2(X);

Float32Array.propDotSelfPositive1 = X => {
    Check.isFloat32Array(X, 'Float32Array.propDotSelfPositive1: X');

    return X.dot(X) >= 0;
};

Float32Array.propDotSelfPositive2 = X => {
    Check.isFloat32Array(X, 'Float32Array.propDotSelfPositive2: X');

    return !(X.dot(X) === 0 || X.approx(new Float32Array(X.length)));
};

Float32Array.propMultiplicativeScalarIdentity = X => {
    Check.isFloat32Array(X, 'Float32Array.propMultiplicativeScalarIdentity: X');

    return X.mul(1).approx(X);
};

Float32Array.propNormFromDot = X => {
    Check.isFloat32Array(X, 'Float32Array.propNormFromDot: X');

    return Number.approx(X.norm, X.dot(X) ** 0.5);
};

Float32Array.propNormTriangleInequality = (X, Y) => {
    Check.isFloat32Array(X, 'Float32Array.propNormTriangleInequality: X');

    Check.isFloat32Array(Y, 'Float32Array.propNormTriangleInequality: Y');

    return X.add(Y).norm <= X.norm + Y.norm;
};

Float32Array.propOrthogonalZeroDot = (X, Y) => {
    Check.isFloat32Array(X, 'Float32Array.propOrthogonalZeroDot: X');

    Check.isFloat32Array(Y, 'Float32Array.propOrthogonalZeroDot: Y');

    const Z = new Float32Array(X.length);

    return (
        (X.approx(Z) && Y.approx(Z)) || !(X.isOrthogonal(Y) || X.dot(Y) === 0)
    );
};

Float32Array.propParsevalIdentity = X => {
    Check.isFloat32Array(X, 'Float32Array.propParsevalIdentity: X');

    return Number.approx(X.norm ** 2, X.analysis.energy);
};

Float32Array.propPythagoreanTheorem = (X, Y) => {
    Check.isFloat32Array(X, 'Float32Array.propPythagoreanTheorem: X');

    Check.isFloat32Array(Y, 'Float32Array.propPythagoreanTheorem: Y');

    return (
        !X.isOrthogonal(Y) ||
        Number.approx(X.add(Y).norm ** 2, X.norm ** 2 + Y.norm ** 2)
    );
};

Float32Array.rec = R => (
    N,
    f,
    {A = 1, d = 0.5, fm = new Float32Array(N), p = 0} = {}
) => {
    Check.isIndex(N, 'Float32Array.rec: N');

    Check.isFinite(f, 'Float32Array.rec: f');

    Check.isFloat32Array(fm, 'Float32Array.rec: fm');

    if (Number.isFinite(A)) {
        A = new Float32Array(N).fill(A);
    } else if (!(A instanceof Float32Array)) {
        throw new TypeError(
            '<Float32Array.rec: A> is not a finite number or a Float32Array'
        );
    }

    const AN = A.length;

    let dN;

    if (Number.isFinite(d)) {
        dN = N;

        d = new Float32Array(N).fill(d % 1);
    } else if (d instanceof Float32Array) {
        dN = d.length;

        d = Float32Array.generate(dN, n => d[n] % 1);
    } else {
        throw new TypeError(
            '<Float32Array.rec: d> is not a finite number or a Float32Array'
        );
    }

    const fmN = fm.length;

    if (Number.isFinite(p)) {
        p = new Float32Array(N).fill(p / TAU);
    } else if (A instanceof Float32Array) {
        p = p.mul(TAU1);
    } else {
        throw new TypeError(
            '<Float32Array.rec: p> is not a finite number or a Float32Array'
        );
    }

    const pN = p.length;

    const a = f / R;

    return Float32Array.generate(N, n => {
        const b = A[n % AN];

        return (a * n + p[n % pN] + fm[n % fmN]) % 1 < d[n % dN] ? b : -b;
    });
};

Float32Array.saw = R => (
    N,
    f,
    {A = 1, fm = new Float32Array(N), p = 0} = {}
) => {
    Check.isIndex(N, 'Float32Array.saw: N');

    Check.isFinite(f, 'Float32Array.saw: f');

    Check.isFloat32Array(fm, 'Float32Array.saw: fm');

    if (Number.isFinite(A)) {
        A = new Float32Array(N).fill(A);
    } else if (!(A instanceof Float32Array)) {
        throw new TypeError(
            '<Float32Array.saw: A> is not a finite number or a Float32Array'
        );
    }

    const AN = A.length;

    const fmN = fm.length;

    if (Number.isFinite(p)) {
        p = new Float32Array(N).fill(p / TAU);
    } else if (A instanceof Float32Array) {
        p = p.mul(TAU1);
    } else {
        throw new TypeError(
            '<Float32Array.saw: p> is not a finite number or a Float32Array'
        );
    }

    const pN = p.length;

    const a = f / R;

    return Float32Array.generate(N, n => {
        const b = a * n + p[n % pN] + fm[n % fmN];

        return A[n % AN] * (b - Math.floor(b) - 0.5);
    });
};

Float32Array.sin = R => (
    N,
    f,
    {A = 1, fm = new Float32Array(N), p = 0} = {}
) => {
    Check.isIndex(N, 'Float32Array.sin: N');

    Check.isFinite(f, 'Float32Array.sin: f');

    Check.isFloat32Array(fm, 'Float32Array.sin: fm');

    if (Number.isFinite(A)) {
        A = new Float32Array(N).fill(A);
    } else if (!(A instanceof Float32Array)) {
        throw new TypeError(
            '<Float32Array.sin: A> is not a finite number or a Float32Array'
        );
    }

    const AN = A.length;

    const fmN = fm.length;

    if (Number.isFinite(p)) {
        p = new Float32Array(N).fill(p / TAU);
    } else if (A instanceof Float32Array) {
        p = p.mul(TAU1);
    } else {
        throw new TypeError(
            '<Float32Array.sin: p> is not a finite number or a Float32Array'
        );
    }

    const pN = p.length;

    const a = TAU * f / R;

    return Float32Array.generate(
        N,
        n => A[n % AN] * Math.sin(a * n + p[n % pN] + fm[n % fmN])
    );
};

Float32Array.sum = X => {
    const N = X.length;

    const Y = X[0];

    const M = Y.length;

    const Z = Y.clone;

    for (let m = 0; m < M; m++) for (let n = 1; n < N; n++) Z[n] += X[n][m];

    return Z;
};

Float32Array.tri = R => (N, f, {A, fm, p} = {}) =>
    Float32Array.saw(R)(N, f, {A, fm, p}).mapQuick(
        a => 2 * (Math.abs(a) - 0.5)
    );

Float32Array.white = (N, A = 1) => {
    Check.isIndex(N, 'Float32Array.white: N');

    Check.isFinite(A, 'Float32Array.white: A');

    A = typeof A === 'number' ? new Float32Array(N).fill(A * 2) : A.mul(2);

    const AN = A.length;

    return Float32Array.generate(N, n => (Math.random() - 0.5) * A[n % AN]);
};

Float32Array.winBartlett = N => {
    Check.isIndex(N, 'Float32Array.winBartlett: N');

    const M = (N - 1) / 2;

    return Float32Array.generateMirror(N, n => 1 - Math.abs((n - M) / M));
};

Float32Array.winCos = coefficients => N => {
    Check.isIndex(N, 'Float32Array.winCos.<anonymous>: N');

    const a = Math.PI / (N - 1);

    return Float32Array.generateMirror(N, n => {
        let x = coefficients[0];

        const b = a * n;

        const M = coefficients.length;

        for (let i = 0; i < M; i++) {
            if (i % 2) {
                x -= coefficients[i] * Math.cos(i * 2 * b);
            } else {
                x += coefficients[i] * Math.cos(i * 2 * b);
            }
        }

        return x;
    });
};

Float32Array.winBlackman = Float32Array.winCos([
    7938 / 18608,
    9240 / 18608,
    1430 / 18608
]);

Float32Array.winBlackmanHarris = Float32Array.winCos([
    0.35875,
    0.48829,
    0.14128,
    0.01168
]);

Float32Array.winBlackmanNuttall = Float32Array.winCos([
    0.3635819,
    0.4891775,
    0.1365995,
    0.0106411
]);

Float32Array.winFlatTop = Float32Array.winCos([1.0, 1.93, 1.29, 0.388, 0.028]);

Float32Array.winGenNormal = power => {
    Check.isFinite(power, 'Float32Array.winGenNormal: power');

    return (N, σ) => {
        Check.isIndex(N, 'Float32Array.winGenNormal.<anonymous>: N');

        Check.isFinite(σ, 'Float32Array.winGenNormal.<anonymous>: σ');

        const a = (N - 1) / 2;

        return Float32Array.generateMirror(N, n =>
            Math.exp(-(((n - a) / (σ * a)) ** power))
        );
    };
};

Float32Array.winGauss = Float32Array.winGenNormal(2);

Float32Array.winHamming = N => {
    Check.isIndex(N, 'Float32Array.winHamming: N');

    const a = TAU / (N - 1);

    const b = 25 / 46;

    const c = 21 / 46;

    return Float32Array.generateMirror(N, n => b - c * Math.cos(a * n));
};

Float32Array.winHann = N => {
    Check.isIndex(N, 'Float32Array.winHann: N');

    const a = TAU / (N - 1);

    return Float32Array.generateMirror(N, n => 0.5 * (1 - Math.cos(a * n)));
};

Float32Array.winNuttall = Float32Array.winCos([
    0.355768,
    0.487396,
    0.144232,
    0.012604
]);

Float32Array.winParzen = N => {
    Check.isIndex(N, 'Float32Array.winParzen: N');

    const N2 = N / 2;

    const N4 = N / 4;

    const X = new Float32Array(N);

    for (let n = -N2; n < -N4; n++)
        X[Math.round(N2 + n)] = X[Math.round(N2 - n - 1)] =
            2 * (1 - Math.abs(n) / N2) ** 3;

    for (let n = -N4; n < N4; n++)
        X[Math.round(N2 + n)] = 1 - 6 * (n / N2) ** 2 * (1 - Math.abs(n) / N2);

    return X;
};

Float32Array.winPoisson = (N, α) => {
    Check.isIndex(N, 'Float32Array.winPoisson: N');

    Check.isFinite(α, 'Float32Array.winPoisson: α');

    const a = N / 2;

    return Float32Array.generateMirror(N, n =>
        Math.exp(-α * (Math.abs(n - a) / a))
    );
};

Float32Array.winSin = N => {
    Check.isIndex(N, 'Float32Array.winSin: N');

    const a = Math.PI / (N - 1);

    return Float32Array.generateMirror(N, n => Math.sin(a * n));
};

Float32Array.winTukey = (N, α) => {
    Check.isIndex(N, 'Float32Array.winTukey: N');

    Check.isFinite(α, 'Float32Array.winTukey: α');

    α %= 1;

    const a = N - 1;

    return Float32Array.generateMirror(
        N,
        n =>
            n < α / 2 * a
                ? 0.5 * (1 + Math.cos(Math.PI * (2 * n / α / a - 1)))
                : 1
    );
};

Float32Array.winWelch = N => {
    Check.isIndex(N, 'Float32Array.winWelch: N');

    const a = (N - 1) / 2;

    return Float32Array.generateMirror(N, n => 1 - ((n - a) / N) ** 2);
};

Object.defineProperty(Float32Array.prototype, 'abs', {
    get: function() {
        return this.mapQuick(Math.abs);
    }
});

Float32Array.prototype.add = function(X) {
    if (Number.isFinite(X)) return this.mapQuick(x => x + X);

    if (X instanceof Float32Array) return this.zip(X, (x, y) => x + y);

    throw new TypeError(
        '<Float32Array.prototype.add: X> is not a finite number or a Float32Array'
    );
};

Float32Array.prototype.analysis = function() {
    return Float32Array.orthonormalBasis(this.length).mapQuick(a =>
        a.dot(this)
    );
};

Float32Array.prototype.append = function(X) {
    Check.isFloat32Array(X, 'Float32Array.prototype.append: X');

    const N = this.length;

    const Y = new Float32Array(N + X.length);

    Y.set(this, 0);

    Y.set(X, N);

    return Y;
};

Float32Array.prototype.approx = function(X) {
    const N = this.length;

    const M = X.length;

    if (N !== M)
        throw new RangeError(
            '<Float32Array.prototype.approx: this> and X are not of equal length.'
        );

    for (let n = 0; n < N; n++)
        if (Math.abs(this[n] - X[n]) > Number.EPSILON) return false;

    return true;
};

Object.defineProperty(Float32Array.prototype, 'clone', {
    get: function() {
        return new Float32Array(this);
    }
});

Object.defineProperty(Float32Array.prototype, 'cma', {
    get: function() {
        let a = 0;

        return Float32Array.generate(this.length, n => {
            a += this[n];

            return a / (n + 1);
        });
    }
});

Float32Array.prototype.delay = function({delay, feedback, N}) {
    Check.isIndex(delay, 'Float32Array.prototype.delay: delay');

    Check.isFinite(feedback, 'Float32Array.prototype.delay: feedback');

    Check.isIndex(N, 'Float32Array.prototype.delay: N');

    const X = this.padr(N - this.length);

    for (let n = delay; n < N; n++) X[n] += X[n - delay] * feedback;

    return X;
};

Float32Array.delta = function(N) {
    Check.isIndex(N, 'Float32Array.delta: N');

    const X = new Float32Array(N);

    X[0] = 1;

    return X;
};

Float32Array.prototype.distance = function(X) {
    Check.isFloat32Array(X, 'Float32Array.prototype.distance: X');

    return this.sub(X).norm;
};

Float32Array.prototype.div = function(X) {
    if (Number.isFinite(X)) return this.mapQuick(x => x / X);

    if (X instanceof Float32Array) return this.zip(X, (x, y) => x / y);

    throw new TypeError(
        '<Float32Array.prototype.div: X> is not a finite number or a Float32Array'
    );
};

Float32Array.prototype.dot = function(X) {
    Check.isFloat32Array(X, 'Float32Array.prototype.dot: X');

    return this.mul(X).sum;
};

Float32Array.prototype.duck = function(
    X,
    {hold = 0, smoothing = 0, threshold}
) {
    Check.isFloat32Array(X, 'Float32Array.prototype.duck: X');

    Check.isFinite(threshold, 'Float32Array.prototype.duck: threshold');

    Check.isIndex(hold, 'Float32Array.prototype.duck: hold');

    Check.isIndex(smoothing, 'Float32Array.prototype.duck: smoothing');

    return new Float32Array(X.length)
        .sub(X.gate(threshold, hold).sma(smoothing))
        .mul(this);
};

Float32Array.prototype.each = function(f) {
    Check.isFunction(f, 'Float32Array.prototype.each: f');

    for (let n = 0, N = this.length; n < N; n++) f(this[n]);
};

Object.defineProperty(Float32Array.prototype, 'energy', {
    get: function() {
        return this.reduceQuick((x, y) => x + y ** 2);
    }
});

Object.defineProperty(Float32Array.prototype, 'fft', {
    get: function() {
        return new Complex32Array(this).fft;
    }
});

Float32Array.prototype.gate = function(A, hold = 0) {
    Check.isFinite(A, 'Float32Array.prototype.gate: A');

    Check.isIndex(hold, 'Float32Array.prototype.gate: hold');

    let a = 0;

    return this.mapQuick(x => {
        if (Math.abs(x) > A) {
            a = hold;

            return 1;
        }

        if (a > 0) {
            a--;

            return 1;
        }

        return 0;
    });
};

Object.defineProperty(Float32Array.prototype, 'ifft', {
    get: function() {
        return new Complex32Array(this).ifft;
    }
});

Float32Array.prototype.isOrthogonal = function(X) {
    Check.isFloat32Array(X, 'Float32Array.prototype.isOrthogonal: X');

    return this.dot(X) === 0;
};

Float32Array.prototype.kron = function(X) {
    const N = this.length;

    if (X instanceof Float32Array) {
        const M = X.length;

        return Float32Array.generate(
            N * M,
            n => this[Math.floor(n / M)] * X[n % N]
        );
    }

    return new Float32Matrix(1, N, this).kron(X);
};

Float32Array.prototype.ks = function({
    delay,
    feedback = 0.98,
    N,
    smoothing = 2
}) {
    Check.isIndex(N, 'Float32Array.prototype.ks: N');

    Check.isIndex(delay, 'Float32Array.prototype.ks: delay');

    Check.isFinite(feedback, 'Float32Array.prototype.delay: feedback');

    Check.isIndex(smoothing, 'Float32Array.prototype.ks: smoothing');

    const X = this.padr(N - this.length);

    let a = 0;

    for (let n = 0; n < smoothing; n++) a += X[n];

    for (let n = delay; n < N - smoothing; n++) {
        const m = n - delay;

        X[n] += a / smoothing * feedback;

        a -= X[m];

        a += X[m + smoothing] || 0;
    }

    return X;
};

Float32Array.prototype.mapQuick = function(f) {
    Check.isFunction(f, 'Float32Array.prototype.mapQuick: f');

    return Float32Array.generate(this.length, n => f(this[n]));
};

Object.defineProperty(Float32Array.prototype, 'max', {
    get: function() {
        return this.reduceQuick(Math.max);
    }
});

Object.defineProperty(Float32Array.prototype, 'min', {
    get: function() {
        return this.reduceQuick(Math.min);
    }
});

Object.defineProperty(Float32Array.prototype, 'mean', {
    get: function() {
        return this.sum / this.length;
    }
});

Object.defineProperty(Float32Array.prototype, 'median', {
    get: function() {
        const N = this.length / 2;

        const n = N / 2;

        return N % 2 ? this[Math.floor(n)] : (this[n - 1] + this[n]) / 2;
    }
});

Object.defineProperty(Float32Array.prototype, 'mode', {
    get: function() {
        const X = new Map();

        let n = 0;

        let a = 0;

        this.each(x => {
            const b = X[x] || 0;

            X[x] = b + 1;

            if (b > a) {
                a = b;
                n = x;
            }
        });

        return n;
    }
});

Float32Array.prototype.mul = function(X) {
    if (Number.isFinite(X)) return this.mapQuick(x => x * X);

    if (X instanceof Float32Array) return this.zip(X, (x, y) => x * y);

    throw new TypeError(
        '<Float32Array.prototype.mul: X> is not a finite number or a Float32Array'
    );
};

Object.defineProperty(Float32Array.prototype, 'neg', {
    get: function() {
        return this.mapQuick(x => -x);
    }
});

Object.defineProperty(Float32Array.prototype, 'norm', {
    get: function() {
        return Math.sqrt(this.sqr.sum);
    }
});

Float32Array.prototype.normalize = function(A = 1) {
    Check.isFinite(A, 'Float32Array.prototype.normalize: A');

    return this.mul(1 / this.peak * A);
};

Float32Array.prototype.pad = function(N, M) {
    Check.isIndex(N, 'Float32Array.prototype.pad: N');

    Check.isIndex(M, 'Float32Array.prototype.pad: M');

    const X = new Float32Array(this.length + N);

    X.set(this, M);

    return X;
};

Float32Array.prototype.padl = function(N) {
    Check.isIndex(N, 'Float32Array.prototype.padl: N');

    return this.pad(N, N);
};

Float32Array.prototype.padr = function(N) {
    Check.isIndex(N, 'Float32Array.prototype.padr: N');

    return this.pad(N, 0);
};

Object.defineProperty(Float32Array.prototype, 'peak', {
    get: function() {
        return this.abs.max;
    }
});

Object.defineProperty(Float32Array.prototype, 'product', {
    get: function() {
        return this.reduceQuick((x, y) => x * y);
    }
});

Float32Array.prototype.reduceQuick = function(f) {
    Check.isFunction(f, 'Float32Array.prototype.reduceQuick: f');

    const N = this.length;

    let a = this[0];

    for (let n = 1; n < N; n++) a = f(a, this[n]);

    return a;
};

Float32Array.prototype.repeat = function(N) {
    Check.isIndex(N, 'Float32Array.prototype.repeat: N');

    let Y = new Float32Array(this);

    for (let n = 0; n < N; n++) Y = Y.append(this);

    return Y;
};

Object.defineProperty(Float32Array.prototype, 'rms', {
    get: function() {
        return Math.sqrt(this.sqr.sum / this.length);
    }
});

Object.defineProperty(Float32Array.prototype, 'sd', {
    get: function() {
        const a = this.mean;

        return Math.sqrt(this.mapQuick(x => (x - a) ** 2)).mean;
    }
});

Float32Array.prototype.shift = function(N) {
    Check.isSafeInteger(N, 'Float32Array.prototype.shift: N');

    if (N === 0) return this.clone;

    const M = this.length;

    N = -(N % M);

    if (N >= 0) N -= M;

    return this.slice(N).append(this.slice(0, N));
};

Float32Array.prototype.sma = function(N) {
    Check.isIndex(N, 'Float32Array.prototype.sma: N');

    N = Math.floor(N);

    const N2 = Math.floor(N / 2);

    const K = N - N2;

    const X = this.padl(N2).padr(N2);

    const M = X.length;

    const Y = new Float32Array(M);

    let a = X.slice(0, N).sum;

    for (let n = K; n < M; n++) {
        a += X[n + N2] - Y[n - K];

        Y[n] = a / N;
    }

    return Y.slice(N2, M - N2);
};

Object.defineProperty(Float32Array.prototype, 'sqr', {
    get: function() {
        return this.mapQuick(x => x ** 2);
    }
});

Float32Array.prototype.sub = function(X) {
    if (Number.isFinite(X)) return this.mapQuick(x => x - X);

    if (X instanceof Float32Array) return this.zip(X, (x, y) => x - y);

    throw new TypeError(
        '<Float32Array.prototype.sub: X> is not a finite number or a Float32Array'
    );
};

Object.defineProperty(Float32Array.prototype, 'sum', {
    get: function() {
        return this.reduceQuick((x, y) => x + y);
    }
});

Float32Array.prototype.trigger = function(X, {delay = 0, threshold = 1}) {
    Check.isFloat32Array(X, 'Float32Array.prototype.trigger: X');

    Check.isFinite(threshold, 'Float32Array.prototype.trigger: threshold');

    Check.isIndex(delay, 'Float32Array.prototype.trigger: delay');

    const M = this.length;

    const K = X.length;

    const Y = new Float32Array(K);

    let a = 0;

    for (let n = 0; n < K; n++) {
        if (X[n] >= threshold && a === 0 && this.length + n < K) {
            for (let m = 0; m < M; m++) Y[n + m] += this[m];

            a = delay;
        } else if (a > 0) {
            a--;
        }
    }

    return Y;
};

Float32Array.prototype.reindex = function(X) {
    Check.isFloat32Array(X, 'Float32Array.prototype.reindex: X');

    const N = this.length;

    const a = X.peak;

    return X.mapQuick(x => this[Math.floor((x / a + 1) / 2 * N) % N]);
};

Float32Array.prototype.resample = function(N) {
    Check.isIndex(N, 'Float32Array.prototype.resample: N');

    const M = this.length;

    return Float32Array.generate(Math.ceil(N), n => {
        const m = Math.ceil(n / N * M);

        const a = m % 1;

        const l = this[m];

        const r = this[m + 1];

        if (typeof l === 'undefined') return 0;

        if (typeof r === 'undefined') return l * a;

        return l * a + r * (1 - a);
    });
};

Float32Array.prototype.zip = function(X, f) {
    Check.isFloat32Array(X, 'Float32Array.prototype.zip: X');

    Check.isFunction(f, 'Float32Array.prototype.zip: f');

    const N = this.length;

    const M = X.length;

    const [Y, Z, K] = N >= M ? [this.clone, X, M] : [X.clone, this, N];

    for (let n = 0; n < K; n++) Y[n] = f(Y[n], Z[n]);

    return Y;
};
