Number.approx = (a, b) => Math.abs(a - b) < Number.EPSILON;
