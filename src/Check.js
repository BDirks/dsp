/* global Complex, Complex32Array, Float32Matrix */

class Check {
    static isComplex(x, name) {
        if (!Complex.isComplex(x))
            throw new TypeError(`<${name}> is not complex`);
    }

    static isComplex32Array(x, name) {
        if (!(x instanceof Complex32Array))
            throw new TypeError(`<${name}> is not a Complex32Array`);
    }

    static isFinite(x, name) {
        if (!Number.isFinite(x)) {
            const expectation = 'Expecting a finite number.';

            if (typeof x === 'number') {
                throw new RangeError(
                    `${expectation} <${name}> (${x}) is not finite`
                );
            } else if (typeof x === 'undefined') {
                throw new TypeError(`${expectation} <${name}> is undefined`);
            } else {
                throw new TypeError(`${expectation} <${name}> is not a number`);
            }
        }
    }

    static isFloat32Array(x, name) {
        if (!(x instanceof Float32Array))
            throw new TypeError(`<${name}> is not a Float32Array`);
    }

    static isFunction(x, name) {
        if (!(x instanceof Function))
            throw new TypeError(`<${name}> is not a function`);
    }

    static isIndex(x, name) {
        Check.isSafeInteger(x, name);

        if (x < 0) throw new RangeError(`<${name}> is negative`);
    }

    static isFloat32Matrix(x, name) {
        if (!Float32Matrix.isFloat32Matrix(x))
            throw new TypeError(`<${name}> is not a Float32Matrix`);
    }

    static isSafeInteger(x, name) {
        if (!Number.isSafeInteger(x))
            throw new TypeError(`<${name}> is not a safe integer`);
    }

    static isString(x, name) {
        if (typeof x !== 'string')
            throw new TypeError(`<${name}> is not a string`);
    }
}
