/* global Check */

class Float32Matrix {
    constructor(N, M, array = new Float32Array(N * M).fill(0)) {
        Check.isIndex(N, 'Float32Matrix.constructor: N');

        Check.isIndex(M, 'Float32Matrix.constructor: M');

        this.N = N;

        this.M = M;

        this.length = N * M;

        if (Array.isArray(array)) array = new Float32Array(array);

        Check.isFloat32Array(array, 'Float32Matrix.constructor.array');

        if (array.length !== this.length)
            throw new RangeError(
                `<Float32Matrix.constructor: array> does not fit size ${N}×${M}`
            );

        this.array = array;
    }

    add(X) {
        Check.isFloat32Matrix(X, 'Float32Matrix.prototype.add: X');

        this.Check.isEqualDimensions(X, 'Float32Matrix.prototype.add: X');

        return this.apply(Y => Y.add(X.array));
    }

    apply(f) {
        Check.isFunction(f, 'Matrix.prototype.apply: f');

        return new Float32Matrix(this.N, this.M, f(this.array));
    }

    get clone() {
        return new Float32Matrix(this.N, this.M, this.array);
    }

    col(m) {
        Check.isIndex(m, 'Float32Matrix.prototype.col: m');

        const M = this.M;

        if (m >= this.M)
            throw new RangeError('<Float32Matrix.prototype.col: m> >= M');

        return Float32Array.generate(this.N, n => this.array[n * M + m]);
    }

    get det() {
        const N = this.N;

        const M = this.M;

        if (N !== M)
            throw new RangeError(
                '<Float32Matrix.prototype.det: this> is not square'
            );

        const X = this.array;

        if (N === 2) return X[0] * X[3] - X[1] * X[2];

        const K = N - 1;

        const L = K * K;

        let a = 0;

        let x = 1;

        for (let m = 0; m < M; m++) {
            const b = X[m];

            const Y = new Float32Array(L);

            let i = 0;

            for (let n = 1; n < N; n++)
                for (let k = 0; k < M; k++)
                    if (k !== m) {
                        Y[i] = X[n * M + k];

                        i++;
                    }

            a += x * b * new Float32Matrix(K, K, Y).det;

            x = -x;
        }

        return a;
    }

    get dims() {
        return [this.N, this.M];
    }

    get isSquare() {
        return this.N === this.M;
    }

    get isSymmetrical() {
        const [N, M] = this.dims;

        if (N !== M) return false;

        for (let m = 0; m < M; m++)
            for (let n = 0; n < N; n++)
                if (this.array[n * M + m] !== this.array[m * M + n])
                    return false;

        return true;
    }

    get isToeplitz() {
        const [N, M] = this.dims;

        if (N !== M) return false;

        const a = this.array[0];

        for (let m = 1; m < M; m++)
            if (this.array[m * M + m] !== a) return false;

        return true;
    }

    kron(X) {
        if (X instanceof Float32Array) X = new Float32Matrix(1, X.length, X);

        Check.isFloat32Matrix(X, 'Float32Matrix.prototype.kron: X');

        const [N0, M0] = this.dims;

        const [N1, M1] = X.dims;

        const N = N0 * N1;

        const M = M0 * M1;

        const Y = new Float32Array(N * M);

        for (let m = 0; m < M; m++)
            for (let n = 0; n < N; n++)
                Y[n * M + m] =
                    this.array[Math.floor(n / N1) * M0 + Math.floor(m / M1)] *
                    X.array[n % N1 * M1 + m % M1];

        return new Float32Matrix(N, M, Y);
    }

    mul(X) {
        if (Number.isFinite(X)) return this.apply(Y => Y.mul(X));

        if (X instanceof Float32Matrix) {
            const N = this.N;

            const M = X.M;

            if (this.M !== X.N)
                throw new RangeError(
                    `<Float32Matrix.prototype.mul: X> can not be multiplied with this Float32Matrix`
                );

            return Float32Matrix.generate(N, M, (m, n) =>
                this.row(n).dot(X.col(m))
            );
        }

        throw new TypeError(
            '<Float32Matrix.prototype.mul: X> is not a finite number or a Float32Matrix'
        );
    }

    pow(n) {
        Check.isIndex(n, 'Float32Matrix.prototype.pow');

        let X = this.clone;

        for (; n >= 2; n--) X = this.mul(X);

        return X;
    }

    row(n) {
        Check.isIndex(n, 'Float32Matrix.prototype.row: n');

        if (n >= this.N) throw new RangeError('<DSP.prototype.row: n> >= N');

        const M = this.M;

        const k = n * M;

        return this.array.slice(k, k + M);
    }

    sub(X) {
        Check.isFloat32Matrix(X, 'Float32Matrix.prototype.sub: X');

        this.checkEqualDimensions(X, 'Float32Matrix.prototype.sub: X');

        return this.apply(Y => Y.sub(X.array));
    }

    get T() {
        const M = this.M;

        return Float32Matrix.generate(
            M,
            this.N,
            (m, n) => this.array[m * M + n]
        );
    }

    static fromCols(X) {
        const N = X[0].length;

        const M = X.length;

        for (let m = 0; m < M; m++)
            if (X[m].length !== N)
                throw new RangeError(
                    `<Float32Matrix.prototype.det: X[${m}]length> is not equal to N`
                );

        return Float32Matrix.generate(N, M, (m, n) => X[m][n]);
    }

    static fromRows(X) {
        const N = X.length;

        const M = X[0].length;

        for (let n = 0; n < N; n++)
            if (X[n].length !== M)
                throw new RangeError(
                    `<Float32Matrix.prototype.det: X[${n}]length> is not equal to N`
                );

        const Y = new Float32Array(N * M);

        for (let n = 0; n < N; n++) Y.set(X[n], n * M);

        return new Float32Matrix(N, M, Y);
    }

    static generate(N, M, f) {
        const X = new Float32Array(N * M);

        for (let m = 0; m < M; m++)
            for (let n = 0; n < N; n++) X[n * M + m] = f(m, n);

        return new Float32Matrix(N, M, X);
    }

    static I(N) {
        Check.isIndex(N, 'Float32Matrix.I: N');

        const M = N * N;

        const X = new Float32Array(M).fill(0);

        const K = N + 1;

        for (let n = 0; n < M; n += K) X[n] = 1;

        return new Float32Matrix(N, N, X);
    }

    static isFloat32Matrix(x) {
        return x instanceof Float32Matrix;
    }
}
