/* global Complex32Array, SVG */

class Plot {
    constructor(X, id, {height = 300, padding = 20} = {}) {
        this.padding = padding;

        this.height = height;

        this.colors = ['#f00', '#00f', '#0f0', '#f0f', '#0ff', '#ff0'];

        document.body.innerHTML += `<svg id='${id}'></svg>`;

        this.SVG = SVG(id).size('100%', this.height + this.padding);

        this.width = this.SVG.node.clientWidth;

        this.plot(X);
    }

    plot(X) {
        let Y = [];

        if (X instanceof Float32Array) {
            Y = [X];
        } else if (X instanceof Complex32Array) {
            Y = [X.re, X.im];
        } else {
            for (const x of X) {
                if (x instanceof Float32Array) {
                    Y.push(x);
                } else {
                    Y.push(x.re);

                    Y.push(x.im);
                }
            }
        }

        const N = Y.length;

        for (let n = 0; n < N; n++) this.draw(Y[n], this.colors[n]);
    }

    draw(array, fill) {
        const h2 = this.height / 2;

        const points = [];

        const step = array.length / this.width;

        for (let n = 0; n < this.width; n++) {
            const i = Math.floor(n * step);

            points.push(
                `${n},${this.height - (h2 + array[i] * h2) + this.padding / 2}`
            );
        }

        this.SVG.polyline(points.join(',')).fill('transparent').stroke({
            color: fill,
            width: 2,
            opacity: 0.5
        });
    }
}
