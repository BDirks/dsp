/* global Check */

class WebAudio {
    constructor() {
        this.context = new window.AudioContext();

        this.rate = this.context.sampleRate;
    }

    cos() {
        return Float32Array.cos(this.rate)(...arguments);
    }

    createBuffer(channels) {
        if (!Array.isArray(channels) || !channels.length)
            throw new TypeError(
                '<WebAudio.prototype.createBuffer: channels> is not an Array of length > 0'
            );

        const N = channels.length;

        const M = channels.reduce((x, y) => Math.min(x.length, y.length));

        const buffer = this.context.createBuffer(N, M, this.rate);

        for (let i = 0; i < N; i++) buffer.copyToChannel(channels[i], i, 0);

        return buffer;
    }

    file(path) {
        Check.isString(path, 'WebAudio.prototype.file: path');

        return new Promise(resolve => {
            const request = new XMLHttpRequest();

            request.open('GET', path, true);

            request.responseType = 'arraybuffer';

            request.onload = () => {
                this.context.decodeAudioData(request.response, buffer => {
                    const N = buffer.numberOfChannels;

                    const channels = new Array(N);

                    for (let n = 0; n < N; n++)
                        channels.push(buffer.getChannelData(n));

                    resolve(channels);
                });
            };

            request.send();
        });
    }

    files(paths) {
        return Promise.all(paths.map(path => this.file(path)));
    }

    ms(n) {
        Check.isFinite(n, 'WebAudio.prototype.ms: N');

        return Math.floor(n * this.rate / 1000);
    }

    play(channels) {
        if (!Array.isArray(channels) || !channels.length)
            throw new TypeError(
                '<WebAudio.prototype.play: channels> is not an Array of length > 0'
            );

        const buffer = this.createBuffer(channels);

        const source = this.context.createBufferSource();

        source.buffer = buffer;

        source.connect(this.context.destination);

        source.start();

        return this;
    }

    rec() {
        return Float32Array.rec(this.rate)(...arguments);
    }

    s(n) {
        Check.isFinite(n, 'WebAudio.prototype.s: N');

        return Math.floor(n * this.rate);
    }

    saw() {
        return Float32Array.saw(this.rate)(...arguments);
    }

    sin() {
        return Float32Array.sin(this.rate)(...arguments);
    }

    tri() {
        return Float32Array.tri(this.rate)(...arguments);
    }
}
