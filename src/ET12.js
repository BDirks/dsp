class ET12 {
    constructor(A4 = 440) {
        this.keys = [
            'c',
            'C',
            'd',
            'D',
            'e',
            'f',
            'F',
            'g',
            'G',
            'a',
            'A',
            'b'
        ];

        this.A4 = A4;

        for (const key of this.keys.slice(9)) {
            const name = `${key}0`;

            this[name] = this.keyToHertz(key, 0);

            this[`maj${name}`] = this.maj(this[name]);

            this[`min${name}`] = this.min(this[name]);
        }

        for (let octave = 1; octave <= 7; octave++)
            for (const key of this.keys) {
                const name = `${key}${octave}`;

                this[name] = this.keyToHertz(key, octave);

                this[`maj${name}`] = this.maj(this[name]);

                this[`min${name}`] = this.min(this[name]);
            }

        this.c8 = this.keyToHertz('c', 8);
    }

    chord(f, X) {
        return X.map(a => this.transpose(f, a));
    }

    hertzToIndex(f) {
        return 12 * Math.log2(f / this.A4) + 49;
    }

    indexToHertz(n) {
        return 2 ** ((n - 49) / 12) * this.A4;
    }

    keyToHertz(key, octave) {
        return this.indexToHertz(
            12 * octave + (this.keys.indexOf(key) - 9) % 12 + 1
        );
    }

    transpose(f, n) {
        return this.indexToHertz(this.hertzToIndex(f) + n);
    }

    maj(f) {
        return this.chord(f, [0, 4, 7]);
    }

    min(f) {
        return this.chord(f, [0, 3, 7]);
    }
}
