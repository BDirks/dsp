/* global Check, Float32Array */

class Complex {
    constructor(re = 0, im = 0) {
        Check.isFinite(re, 'Complex.constructor: re');

        Check.isFinite(im, 'Complex.constructor: im');

        this.re = re;

        this.im = im;

        if (this.re === -0) this.re = 0;

        if (this.im === -0) this.im = 0;
    }

    get abs() {
        return this.mag;
    }

    get acos() {
        const {re, im} = this.asin;

        return new Complex(Complex.PI2.sub(re), -im);
    }

    get acosh() {
        const {re, im} = this.acos;

        if (im <= 0) return new Complex(-im, re);

        return new Complex(im, -re);
    }

    add(z) {
        if (Number.isFinite(z)) return new Complex(this.re + z, this.im);

        if (z instanceof Complex)
            return new Complex(this.re + z.re, this.im + z.im);

        throw new TypeError(
            '<Complex.prototype.add: z> is not a (complex) number'
        );
    }

    get angle() {
        return this.phase;
    }

    approx(z) {
        if (Number.isFinite(z)) return this.isReal && Number.approx(this.re, z);

        if (z instanceof Complex)
            return Number.approx(this.re, z.re) && Number.approx(this.im, z.im);

        throw new TypeError(
            '<Complex.prototype.approx: z> is not a (complex) number'
        );
    }

    get arg() {
        return this.phase;
    }

    get asin() {
        const re = this.re;

        if (this.isReal)
            return new Complex(Math.abs(re) > 1 ? Math.PI / 2 : Math.asin(re));

        const sqr = this.sqr;

        const y = new Complex(-this.im, re).add(
            new Complex(1 - sqr.re, -sqr.im).sqrt
        ).log;

        return new Complex(y.im, -y.re);
    }

    get asinh() {
        const re = this.re;

        if (this.isReal)
            return new Complex(Math.abs(re) > 1 ? Math.PI / 2 : Math.asinh(re));

        return this.add(this.sqr.add(1).sqrt).log;
    }

    get cis() {
        const re = this.re;

        const p = Math.exp(-this.im);

        return new Complex(p * Math.cos(re), p * Math.sin(re));
    }

    get clone() {
        return new Complex(this.re, this.im);
    }

    get conj() {
        return new Complex(this.re, -this.im);
    }

    get cos() {
        const {re, im} = this;

        return new Complex(
            Math.cos(re) * Math.cosh(im),
            -Math.sin(re) * Math.sinh(im)
        );
    }

    get cosh() {
        const {re, im} = this;

        return new Complex(
            Math.cosh(re) * Math.cos(im),
            Math.sinh(re) * Math.sin(im)
        );
    }

    div(z) {
        if (Number.isFinite(z))
            return Complex.fromPolar(this.mag / z, this.phase);

        if (z instanceof Complex) {
            const a = this.re;

            const b = this.im;

            const c = z.re;

            const d = z.im;

            const p = Math.hypot(c, d);

            return new Complex((a * c + b * d) / p, (b * c - a * d) / p);
        }

        throw new TypeError(
            '<Complex.prototype.div: z> is not a (complex) number'
        );
    }

    get exp() {
        const im = this.im;

        const p = Math.exp(this.re);

        return new Complex(p * Math.cos(im), p * Math.sin(im));
    }

    get inv() {
        return new Complex(-this.re, -this.im);
    }

    get isFinite() {
        return isFinite(this.re) && isFinite(this.im);
    }

    get isImag() {
        return Number.approx(this.re, 0);
    }

    get isInfinite() {
        return !this.isFinite;
    }

    get isNaN() {
        return isNaN(this.re) || isNaN(this.im);
    }

    get isReal() {
        return Number.approx(this.im, 0);
    }

    get isZero() {
        return Number.approx(this.re, 0) && Number.approx(this.im, 0);
    }

    get ln() {
        return this.log;
    }

    get log() {
        return new Complex(Math.log(this.mag), this.phase);
    }

    get log10() {
        return this.log.mul(Math.LOG10E);
    }

    get mag() {
        return Math.hypot(this.re, this.im);
    }

    get mod() {
        return this.mag;
    }

    mul(z) {
        if (Number.isFinite(z)) return new Complex(this.re * z, this.im);

        if (z instanceof Complex) {
            const a = this.re;

            const b = this.im;

            const c = z.re;

            const d = z.im;

            return new Complex(a * c - b * d, a * d + b * c);
        }

        throw new TypeError(
            '<Complex.prototype.mul: z> is not a (complex) number'
        );
    }

    get neg() {
        return this.inv;
    }

    get phase() {
        return Math.atan2(this.im, this.re);
    }

    pow(z) {
        Check.isComplex(z, 'Complex.prototype.pow: z');

        const zRe = z.re;

        if (z.isZero) return new Complex(1);

        if (this.isZero) {
            if (zRe > 0) return new Complex();

            if (zRe < 0) return new Complex(Infinity);

            return Complex.NaN();
        }

        if (this.isFinite) {
            if (zRe > 0) return new Complex(Infinity);

            if (zRe < 0) return new Complex();

            return Complex.NaN();
        }

        return new Complex(this.log.mul(z)).exp;
    }

    get recip() {
        if (this.isZero) return Complex.NaN;

        const {re, im} = this;

        const p = Math.hypot(re, im);

        return new Complex(re / p, -im / p);
    }

    get round() {
        return new Complex(Math.round(this.re), Math.round(this.im));
    }

    get sign() {
        return this.signum;
    }

    get signum() {
        return Math.sign(this.isImag ? this.im : this.re);
    }

    get sin() {
        const {re, im} = this;

        return new Complex(
            Math.sin(re) * Math.cosh(im),
            Math.cos(re) * Math.sinh(im)
        );
    }

    get sinh() {
        const {re, im} = this;

        return new Complex(
            Math.sinh(re) * Math.cos(im),
            Math.cosh(re) * Math.sin(im)
        );
    }

    get sqr() {
        return this.mul(this);
    }

    get sqrt() {
        if (this.isZero) return this;

        const {re, im, mag} = this;

        const p = Math.sqrt((re + mag) / 2);

        const q = Math.sqrt((-re + mag) / 2);

        return new Complex(p, Math.sign(im) * q);
    }

    sub(z) {
        if (Number.isFinite(z)) return new Complex(this.re - z, this.im);

        if (z instanceof Complex)
            return new Complex(this.re - z.re, this.im - z.im);

        throw new TypeError(
            '<Complex.prototype.sub: z> is not a (complex) number'
        );
    }

    get tan() {
        return this.sin.div(this.cos);
    }

    get tanh() {
        return this.sinh.div(this.cosh);
    }

    static get E() {
        return new Complex(Math.E);
    }

    static fromPolar(mag, phase) {
        Check.isFinite(mag, 'Complex.prototype.fromPolar: mag');

        Check.isFinite(phase, 'Complex.prototype.fromPolar: phase');

        return new Complex(mag * Math.cos(phase), mag * Math.sin(phase));
    }

    static get I() {
        return new Complex(0, 1);
    }

    static isComplex(x) {
        return x instanceof Complex;
    }

    static get NaN() {
        return new Complex(NaN, NaN);
    }

    static get PI() {
        return new Complex(Math.PI);
    }

    static get PI2() {
        return new Complex(Math.PI / 2);
    }
}
