/* global Check, Complex */

class Complex32Array {
    constructor(re, im) {
        if (typeof re === 'number') {
            this.length = re;

            this.re = new Float32Array(this.length);

            this.im = new Float32Array(this.length);
        } else if (re instanceof Float32Array) {
            this.length = re.length;

            this.re = re;

            if (im instanceof Float32Array) {
                if (im.length === re.length) {
                    this.im = im;
                } else {
                    throw new RangeError(
                        '<Complex32Array.constructor: im> and re are not of equal length'
                    );
                }
            } else if (typeof im === 'undefined') {
                this.im = new Float32Array(this.length);
            } else {
                throw new TypeError(
                    '<Complex32Array.constructor: im> is not a Float32Array or undefined'
                );
            }
        } else {
            throw new TypeError(
                '<Complex32Array.constructor: im> is not a Float32Array or a number'
            );
        }
    }

    get abs() {
        return this.re.zip(this.im, (x, y) => Math.hypot(x, y));
    }

    add(X) {
        if (typeof X === 'number' || X instanceof Float32Array)
            return this.bimap(Y => Y.add(X));

        if (X instanceof Complex || X instanceof Complex32Array)
            return this.bimap(Y => Y.add(X.re), Y => Y.add(X.im));

        throw new TypeError(
            '<Complex32Array.prototype.add: X> is not a (complex) number, a Float32Array, or a Complex32Array'
        );
    }

    append(X) {
        Check.isComplex32Array(X, 'Complex32Array.prototype.append: X');

        return new Complex32Array(this.re.append(X.re), this.im.append(X.im));
    }

    bimap(f, g = f) {
        Check.isFunction(f, 'Complex32Array.prototype.bimap: f');

        Check.isFunction(g, 'Complex32Array.prototype.bimap: g');

        const X = this.clone;

        return new Complex32Array(f(X.re), g(X.im));
    }

    get clone() {
        return new Complex32Array(this.re.clone, this.im.clone);
    }

    get conj() {
        return new Complex32Array(this.re, this.im.neg);
    }

    conv(X) {
        Check.isComplex32Array(X, 'Complex32Array.prototype.conv: X');

        const N = this.length;

        const M = X.length;

        const K = Math.max(N, M);

        const L = 2 ** Math.ceil(Math.log2(K));

        return this.padr(L - N).fft.mul(X.padr(L - M).fft).slice(0, N + M - 1)
            .ifft;
    }

    dot(X) {
        Check.isComplex32Array(X, 'Complex32Array.prototype.dot: X');

        return this.conj.mul(X).sum;
    }

    filter(f) {
        Check.isFunction(f, 'Complex32Array.prototype.filter: f');

        const N = this.length;

        return this.fft.mul(f(N)).ifft.slice(0, N);
    }

    get fft() {
        let X = this.clone;

        let N = this.length;

        const p = Math.log2(N);

        if (!Number.isInteger(p)) X = X.padr(2 ** Math.ceil(p) - N);

        const f = X => {
            let N = X.length;

            if (N <= 1) return X;

            const N2 = N / 2;

            let even = new Complex32Array(N2);

            let odd = new Complex32Array(N2);

            for (let n = 0; n < N2; ++n) {
                let m = n * 2;

                even.re[n] = X.re[m];

                even.im[n] = X.im[m];

                m++;

                odd.re[n] = X.re[m];

                odd.im[n] = X.im[m];
            }

            even = f(even);

            odd = f(odd);

            const a = 2 * Math.PI;

            for (let k = 0; k < N2; ++k) {
                const bR = even.re[k];

                const bI = even.im[k];

                const b = a * k / N;

                const cR = Math.cos(b);

                const cI = -Math.sin(b);

                const dR = odd.re[k];

                const dI = odd.im[k];

                const eR = cR * dR - cI * dI;

                const eI = cR * dI + cI * dR;

                X.re[k] = bR + eR;

                X.im[k] = bI + eI;

                const l = k + N2;

                X.re[l] = bR - eR;

                X.im[l] = bI - eI;
            }

            return X;
        };

        return f(X);
    }

    get ifft() {
        return this.swap.fft.swap.mul(1 / this.length);
    }

    map(f) {
        Check.isFunction(f, 'Complex32Array.prototype.map: f');

        const X = this.clone;

        const N = this.length;

        for (let n = 0; n < N; n++) [X.re[n], X.im[n]] = f(X.re[n], X.im[n]);

        return X;
    }

    mul(X) {
        if (typeof X === 'number' || X instanceof Float32Array)
            return this.bimap(Y => X.mul(Y));

        if (X instanceof Complex) {
            const N = this.length;

            const c = X.re;

            const d = X.im;

            const Y = new Complex32Array(N);

            for (let n = 0; n < N; n++) {
                const a = this.re[n];

                const b = this.im[n];

                Y.re[n] = a * c - b * d;

                Y.im[n] = a * d + b * c;
            }

            return Y;
        }

        if (X instanceof Complex32Array) {
            const N = this.length;

            const M = X.length;

            const A = this.clone;

            const B = X.clone;

            const [Y, Z, K] = N > M ? [A, B, M] : [B, A, N];

            for (let n = 0; n < K; n++) {
                const a = Y.re[n];

                const b = Y.im[n];

                const c = Z.re[n];

                const d = Z.im[n];

                Y.re[n] = a * c - b * d;

                Y.im[n] = a * d + b * c;
            }

            return Y;
        }

        throw new TypeError(
            '<Complex32Array.prototype.mul: X> is not a (complex) number, a Float32Array, or a Complex32Array'
        );
    }

    padl(N) {
        Check.isIndex(N, 'Complex32Array.prototype.padl: N');

        return this.bimap(X => X.padl(N));
    }

    padr(N) {
        Check.isIndex(N, 'Complex32Array.prototype.padr: N');

        return this.bimap(X => X.padr(N));
    }

    get reverse() {
        return this.bimap(X => X.reverse);
    }

    shift(N) {
        return this.bimap(X => X.shift(N));
    }

    sub(X) {
        if (typeof X === 'number' || X instanceof Float32Array)
            return this.bimap(Y => Y.add(X));

        if (X instanceof Complex || X instanceof Complex32Array)
            return this.bimap(re => re.sub(X.re), im => im.sub(X.im));

        throw new TypeError(
            '<Complex32Array.prototype.sub: X> is not a (complex) number, a Float32Array, or a Complex32Array'
        );
    }

    get sum() {
        return this.bimap(X => X.sum);
    }

    get swap() {
        return new Complex32Array(this.im, this.re);
    }

    zip(X, f) {
        Check.isComplex32Array(X, 'Complex32Array.prototype.zip: X');

        Check.isFunction(f, 'Complex32Array.prototype.zip: f');

        const N = this.length;

        const M = X.length;

        const A = this.clone;

        const B = X.clone;

        const [Y, Z, K] = N > M ? [A, B, M] : [B, A, N];

        for (let n = 0; n < K; n++)
            [Y.re[n], Y.im[n]] = f(Y.re[n], Y.im[n], Z.re[n], Z.im[n]);

        return Y;
    }

    static delta(N) {
        return new Complex32Array(Float32Array.delta(N));
    }

    static exp() {
        return new Complex32Array(
            Float32Array.cos(...arguments),
            Float32Array.sin(...arguments)
        );
    }

    static expDecay(N, a) {
        Check.isIndex(N, 'Complex32Array.expDecay: N');

        Check.isComplex(a, 'Complex32Array.expDecay: a');

        if (a.abs > 1)
            throw new RangeError(
                '<Complex32Array.expDecay: a> has an absolute value greater than 1'
            );

        const X = new Complex32Array(N);

        X.re[0] = a.re;

        X.im[0] = a.im;

        for (let n = 1; n < N; n++) {
            const re = X.re[n - 1];

            const im = X.im[n - 1];

            X.re[n] = re * re - im * im;

            X.im[n] = 2 * re * im;
        }

        return X;
    }
}
